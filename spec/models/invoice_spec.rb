require 'rails_helper'

describe Invoice do
  context "validation" do
    it {should validate_presence_of :invoice_ref}
    it {should validate_presence_of :invoice_date}
    it {should validate_presence_of :super_total}
    it {should validate_presence_of :gst_included}
    it {should validate_presence_of :invoice_total}
    it {should validate_presence_of :client_id}
    it {should validate_presence_of :booking_id}
  end

  context "relationships" do
    #it {should have_many :invoice_details}
    it {should belong_to :booking}
    it {should belong_to :client}
  #  it {should accept_nested_attributes_for :invoice_details}
  end
end
