require 'rails_helper'

describe Client do
  context "validation" do
    it {should validate_presence_of :client_name}
    it {should validate_presence_of :street_address}
    it {should validate_presence_of :street_city}
    it {should validate_presence_of :street_state}
    it {should validate_presence_of :street_postcode}
    it {should validate_presence_of :contact_name}
    it {should validate_presence_of :contact_phone}
    it {should validate_presence_of :contact_email}
    it {should validate_presence_of :client_abn}
  end

  context "relationships" do
    it {should have_many :invoices}
    it {should have_many :invoice_payments}
    it {should have_many :bookings}
  end

end
