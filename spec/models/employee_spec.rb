require 'rails_helper'

describe Employee, type: :model do
  context "validation" do
    it {should validate_presence_of :first_name}
    it {should validate_presence_of :surname}
    it {should validate_presence_of :full_name}
    it {should validate_presence_of :address}
    it {should validate_presence_of :city}
    it {should validate_presence_of :postcode}
    it {should validate_presence_of :state}
    it {should validate_presence_of :home_phone}
    it {should validate_presence_of :mobile_phone}
  end

  context "relationships" do
    it {should have_many :payments}
    it {should have_many :invoice_details}
    it {should have_many :booking_details}
  end
end