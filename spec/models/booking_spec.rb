require 'rails_helper'

describe Booking do
  context "validation" do
    it {should validate_presence_of :booking_ref}
    it {should validate_presence_of :start_date}
    it {should validate_presence_of :end_date}
    it {should validate_presence_of :description}
    it {should validate_presence_of :duration}
    it {should validate_presence_of :client_id}
  end

  context "relationships" do
    it {should have_many :booking_details}
    it {should belong_to :client}
    it {should accept_nested_attributes_for :booking_details}
  end
end
