require 'rails_helper'

describe BookingDetail do
  context "validation" do
    it {should validate_presence_of :start_date}
    it {should validate_presence_of :end_date}
    it {should validate_presence_of :duration}
    it {should validate_presence_of :notes}
    it {should validate_presence_of :employee_id}
  end

  context "relationships" do
    it {should belong_to :booking}
    it {should belong_to :employee}
  end
end
