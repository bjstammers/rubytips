require 'rails_helper'

RSpec.feature "Employees", type: :feature do
  before do
    @keyword = FactoryBot.create(:state_admin_keyword)
  end
  before do
    @employee = FactoryBot.create(:employee)
  end

  scenario 'employee index' do
    visit employees_path
    expect(page).to have_content('Name')
    expect(page).to have_content('Address')
    expect(page).to have_content('Home Phone')
    expect(page).to have_content('Mobile Phone')
    expect(page).to have_content('Email')
  end

  feature 'create employee' do
    scenario 'check form structure' do
      visit new_employee_path
      expect(page).to have_link('Details')
      expect(page).to have_link('Contact')
      expect(page).to have_link('Financials')
      expect(page).to have_content('New Talent')
      expect(page).to have_button('Save')
      expect(page).to have_field('First name')
      expect(page).to have_field('Middle name')
      expect(page).to have_field('Surname')
      expect(page).to have_field('Known as')
      expect(page).to have_field('Full name')
      expect(page).to have_field('Birth date')
      expect(page).to have_select('employee_dob_display')
      click_link('Contact')
      expect(page).to have_field('Address')
      expect(page).to have_field('City')
      expect(page).to have_select('employee_state')
      expect(page).to have_field('Postcode')
      expect(page).to have_field('Home phone')
      expect(page).to have_field('Work phone')
      expect(page).to have_field('Mobile phone')
      expect(page).to have_field('Email')
      click_link('Financials')
      expect(page).to have_field('Superannuation fund')
      expect(page).to have_field('Superannuation no')
      expect(page).to have_field('Tax file number')
      expect(page).to have_field('Abn')
      expect(page).to have_select('employee_gst_registered')
    end
    scenario 'populate and submit form' do
      visit new_employee_path
      fill_in 'First name', with: 'Joseph'
      fill_in 'Middle name', with: 'James'
      fill_in 'Surname', with: 'Bloggs'
      fill_in 'Known as', with: 'Joe'
      fill_in 'Full name', with: 'Joe Bloggs'
      fill_in 'Birth date', with: '01/01/1990'
      page.select('No', from: 'employee_dob_display')
      click_link('Contact')
      fill_in 'Address', with: '2 Example Avenue'
      fill_in 'City', with: 'Sydney'
      page.select('ACT', from: 'employee_state')
      fill_in 'Postcode', with: '2121'
      fill_in 'Home phone', with: '(02) 11111111'
      fill_in 'Work phone', with: '(02) 22222222'
      fill_in 'Mobile phone', with: '0433 333333'
      fill_in 'Email', with: 'jb@test.com.au'
      click_link('Financials')
      fill_in 'Superannuation fund', with: 'SuperFund 01'
      fill_in 'Superannuation no', with: '999 888 777'
      fill_in 'Tax file number', with: '333 444 555'
      fill_in 'Abn', with: '123 465 789'
      page.select('Yes', from: 'employee_gst_registered')
      click_button 'Save'
      expect(page).to have_content("success")
    end
   end
  scenario 'edit employee' do
    visit edit_employee_path(@employee.id)
    fill_in 'First name', with: 'Alexander'
    fill_in 'Middle name', with: 'Joseph'
    fill_in 'Surname', with: 'Brown'
    fill_in 'Known as', with: 'Alex'
    fill_in 'Full name', with: 'Alex Brown'
    click_button 'Save'
    expect(page).to have_content("success")
  end

  scenario 'invalid employee' do
    visit new_employee_path
    #fill_in 'First name', with: 'Joseph'
    fill_in 'Middle name', with: 'James'
    #fill_in 'Surname', with: 'Bloggs'
    fill_in 'Known as', with: 'Joe'
    fill_in 'Full name', with: 'Joe Bloggs'
    fill_in 'Birth date', with: '01/01/1990'
    page.select('No', from: 'employee_dob_display')
    click_link('Contact')
    fill_in 'Address', with: '2 Example Avenue'
    fill_in 'City', with: 'Sydney'
    page.select('ACT', from: 'employee_state')
    fill_in 'Postcode', with: '2121'
    fill_in 'Home phone', with: '(02) 11111111'
    fill_in 'Work phone', with: '(02) 22222222'
    fill_in 'Mobile phone', with: '0433 333333'
    fill_in 'Email', with: 'jb@test.com.au'
    click_link('Financials')
    fill_in 'Superannuation fund', with: 'SuperFund 01'
    fill_in 'Superannuation no', with: '999 888 777'
    fill_in 'Tax file number', with: '333 444 555'
    fill_in 'Abn', with: '123 465 789'
    page.select('Yes', from: 'employee_gst_registered')
    click_button 'Save'
    expect(page).to have_content("error")
  end

end
