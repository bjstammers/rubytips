require 'rails_helper'

RSpec.feature "Bookings", type: :feature do
  before do
    @client = FactoryBot.create(:client)
    @employee = FactoryBot.create(:employee)
    @booking = FactoryBot.create(:booking)
  end

  scenario 'bookings index' do
    visit bookings_path
    expect(page).to have_content('Ref #')
    expect(page).to have_content('Client')
    expect(page).to have_content('Start Date')
    expect(page).to have_content('End Date')
    expect(page).to have_content('Description')
  end

  feature 'create booking' do
    scenario 'check form structure' do
      visit new_booking_path
      expect(page).to have_content('New Booking')
      expect(page).to have_button('Save')
      expect(page).to have_select('booking_client_id')
      expect(page).to have_field('Booking ref')
      expect(page).to have_field('Start date')
      expect(page).to have_field('End date')
      expect(page).to have_field('Duration')
      expect(page).to have_field('Description')
      expect(page).to have_select('booking_booking_details_attributes_0_employee_id')
      expect(page).to have_field('booking_booking_details_attributes_0_start_date')
      expect(page).to have_field('booking_booking_details_attributes_0_end_date')
      expect(page).to have_field('booking_booking_details_attributes_0_duration')
      expect(page).to have_field('booking_booking_details_attributes_0_notes')
      # click_link('Add Details')
      # expect(page).to have_select('booking_booking_details_attributes_1_employee_id')
      # expect(page).to have_field('booking_booking_details_attributes_1_start_date')
      # expect(page).to have_field('booking_booking_details_attributes_1_end_date')
      # expect(page).to have_field('booking_booking_details_attributes_1_duration')
      # expect(page).to have_field('booking_booking_details_attributes_1_notes')
    end
    scenario 'populate and submit form' do
      visit new_booking_path
      #save_and_open_page
      page.select('Sample Client 1', from: 'booking_client_id')
      fill_in 'Booking ref', with: 'aaaaaaaaaa'
      fill_in 'Start date', with: '01/01/2018'
      fill_in 'End date', with: '02/01/2018'
      fill_in 'Duration', with: '1 day'
      fill_in 'Description', with: 'BBBBBBBBBBBBBB'
      page.select('Bob Smith 1', from: 'booking_booking_details_attributes_0_employee_id')
      fill_in 'booking_booking_details_attributes_0_start_date', with: '01/01/2018'
      fill_in 'booking_booking_details_attributes_0_end_date', with: '02/01/2018'
      fill_in 'booking_booking_details_attributes_0_duration_date', with: '1 day'
      fill_in 'booking_booking_details_attributes_0_notes', with: 'ccccccccccccc'
      click_link('Add Details')
      page.select('Bob Smith 1', from: 'booking_booking_details_attributes_1_employee_id')
      fill_in 'booking_booking_details_attributes_1_start_date', with: '01/01/2018'
      fill_in 'booking_booking_details_attributes_1_end_date', with: '02/01/2018'
      fill_in 'booking_booking_details_attributes_1_duration_date', with: '1 day'
      fill_in 'booking_booking_details_attributes_1_notes', with: 'ccccccccccccc'
      click_button 'Save'
      expect(page).to have_content("success")
    end
   end
  scenario 'edit booking' do
    visit edit_booking_path(@booking.id)
    save_and_open_page
    fill_in 'Booking ref', with: 'DDDDDDDDDD'
    fill_in 'Start date', with: '02/02/2018'
    fill_in 'End date', with: '04/02/2018'
    fill_in 'Duration', with: '2 days'
    fill_in 'Description', with: 'eeeeeeeeeeeeeeeee'
    click_button 'Save'
    expect(page).to have_content("success")
  end

  scenario 'invalid employee' do
    visit new_booking_path
    page.select('Sample Client 1', from: 'booking_client_id')
    #fill_in 'Booking ref', with: 'aaaaaaaaaa'
    #fill_in 'Start date', with: '01/01/2018'
    fill_in 'End date', with: '02/01/2018'
    fill_in 'Duration', with: '1 day'
    fill_in 'Description', with: 'BBBBBBBBBBBBBB'
    click_button 'Save'
    expect(page).to have_content("error")
  end

end
