require 'rails_helper'

RSpec.feature "Clients", type: :feature do
  before do
    @keyword = FactoryBot.create(:state_admin_keyword)
  end

  scenario 'client index' do
    visit clients_path
    expect(page).to have_content('Clients')
    expect(page).to have_content('Client Name')
    expect(page).to have_content('Address')
    expect(page).to have_content('Contact Name')
    expect(page).to have_content('Phone')
    expect(page).to have_content('Fax')
    expect(page).to have_content('Email')
  end

  feature 'create client' do
    scenario 'check form structure' do
      visit new_client_path
      expect(page).to have_content('Client Details')
      expect(page).to have_button('Save')
      expect(page).to have_field('Client name')
      expect(page).to have_field('Street address')
      expect(page).to have_field('Street city')
      expect(page).to have_select('client_street_state')
      expect(page).to have_field('Street postcode')
      expect(page).to have_field('Postal address')
      expect(page).to have_field('Postal city')
      expect(page).to have_select('client_postal_state')
      expect(page).to have_field('Postal postcode')
      expect(page).to have_field('Contact name')
      expect(page).to have_field('Contact phone')
      expect(page).to have_field('Contact fax')
      expect(page).to have_field('Contact email')
      expect(page).to have_select('client_client_tax')
      expect(page).to have_select('client_client_super')
      expect(page).to have_field('Client abn')
      expect(page).to have_field('Client terms')
    end
    scenario 'populate and submit form' do
      visit new_client_path
      fill_in 'Client name', with: 'Sample client'
      fill_in 'Street address', with: '1 Test Road'
      fill_in 'Street city', with: 'Exampleton'
      page.select('ACT', from: 'client_street_state')
      fill_in 'Street postcode', with: '1001'
      fill_in 'Postal address', with: 'GPO Box 1234'
      fill_in 'Postal city', with: 'Exmapleton'
      page.select('ACT', from: 'client_postal_state')
      fill_in 'Postal postcode', with: '2001'
      fill_in 'Contact name', with: 'Joe Bloggs'
      fill_in 'Contact phone', with: '1111111'
      fill_in 'Contact fax', with: '2222222'
      fill_in 'Contact email', with: 'a@b.com'
      page.select('Yes', from: 'client_client_tax')
      page.select('Yes', from: 'client_client_super')
      fill_in 'Client abn', with: '999999999'
      fill_in 'Client terms', with: '7 days'
      click_button 'Save'
      expect(page).to have_content("success")
    end
  end
  scenario 'edit client' do
    visit edit_client_path(1)
    fill_in 'Client name', with: 'Sample client 2'
    fill_in 'Street address', with: '2 Test Road'
    fill_in 'Street city', with: 'Exampleton 2'
    fill_in 'Street postcode', with: '2002'
    click_button 'Save'
    expect(page).to have_content("success")
  end

  scenario 'invalid client' do
    visit new_client_path
    #fill_in 'Client name', with: 'Sample client 2'
    fill_in 'Street address', with: '2 Test Road'
    fill_in 'Street city', with: 'Exampleton2'
    page.select('ACT', from: 'client_street_state')
    fill_in 'Street postcode', with: '2001'
    fill_in 'Postal address', with: 'GPO Box 2222'
    fill_in 'Postal city', with: 'Exmapleton 2'
    page.select('ACT', from: 'client_postal_state')
    fill_in 'Postal postcode', with: '2002'
    fill_in 'Contact name', with: 'Joe Bloggs 2'
    fill_in 'Contact phone', with: '1111111'
    fill_in 'Contact fax', with: '2222222'
    fill_in 'Contact email', with: 'a@b.com'
    page.select('Yes', from: 'client_client_tax')
    page.select('Yes', from: 'client_client_super')
    fill_in 'Client abn', with: '999999999'
    fill_in 'Client terms', with: '7 days'
    click_button 'Save'
    expect(page).to have_content("error")
  end

end
