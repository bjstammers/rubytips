FactoryBot.define do
    factory :booking_detail do
        start_date 2017-01-01
        end_date 2017-01-03
        duration "14 days"
        notes "Extra text"
        
        association :employee, factory: :assoc_employee
    end
    
    factory :invalid_booking_detail_start_date, parent: :booking  do
        start_date nil
    end
    factory :invalid_booking_detail_end_date, parent: :booking  do
        end_date nil
    end
    factory :invalid_booking_detail_duration, parent: :booking  do
        duration nil
    end
    factory :invalid_notes, parent: :booking  do
        notes nil
    end
end