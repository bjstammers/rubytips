FactoryBot.define do
    factory :employee do
        first_name "Robert"
        middle_name "James"
        surname "Smith"
        full_name "Bob Smith 1"
        known_as "Bob"
        address "2 Test Avenue"
        city "Brisbane"
        state "QLD"
        postcode "4000"
        home_phone "07 3333 1111"
        work_phone "07 3333 2222"
        mobile_phone "0422 222 222"
        email "bsmith@example.com.au"
        birth_date "01-01-1979"
        dob_display "Yes"
        superannuation_fund "SampleSuper"
        superannuation_no "ABC1234"
        tax_file_number "123 456 789"
        abn "9999 88 777"
        gst_registered "No"
    end

  factory :invalid_first_name, parent: :employee  do
    first_name nil
  end
  factory :invalid_surname, parent: :employee  do
    surname nil
  end
  factory :invalid_full_name, parent: :employee  do
    full_name nil
  end
  factory :invalid_address, parent: :employee  do
    address nil
  end
  factory :invalid_city, parent: :employee  do
    city nil
  end
  factory :invalid_state, parent: :employee  do
    state nil
  end
  factory :invalid_postcode, parent: :employee  do
    postcode nil
  end
  factory :invalid_home_phone, parent: :employee  do
    home_phone nil
  end
  factory :invalid_mobile_phone, parent: :employee  do
    mobile_phone nil
  end

end
