FactoryBot.define do
    factory :assoc_booking_details, class: BookingDetail do
        start_date 2017-02-02
        end_date 2017-02-04
        duration "14 days"
        notes "Extra text"

        association :employee, factory: :assoc_employee
    end
end
