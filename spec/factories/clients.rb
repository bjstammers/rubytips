FactoryBot.define do
  factory :client do
    client_name 'Sample Client 1'
    street_address '1 Test Avenue'
    street_city 'Brisbane'
    street_state 'QLD'
    street_postcode '4001'
    postal_address 'GPO Box 1234'
    postal_city 'Brisbane'
    postal_state 'QLD'
    postal_postcode '4000'
    contact_name 'Fred Smith'
    contact_phone '0411 222 333'
    contact_fax '(07) 3333 4444'
    contact_email 'fsmith@sample.com.au'
    client_tax 'Yes'
    client_super 'Yes'
    client_abn '111 22 3333'
    client_terms '7 days'
  end

  factory :invalid_client_name, parent: :client  do
    client_name nil
  end
  factory :invalid_street_address, parent: :client  do
    street_address nil
  end
  factory :invalid_street_city, parent: :client  do
    street_city nil
  end
  factory :invalid_street_state, parent: :client  do
    street_state nil
  end
  factory :invalid_street_postcode, parent: :client  do
    street_postcode nil
  end
  factory :invalid_contact_name, parent: :client  do
    contact_name nil
  end
  factory :invalid_contact_email, parent: :client  do
    contact_email nil
  end
  factory :invalid_contact_phone, parent: :client  do
    contact_phone nil
  end
  factory :invalid_client_abn, parent: :client  do
    client_abn nil
  end

end
