FactoryBot.define do
    factory :admin_keyword do
        key_type 'First'
        key_subtype 'Second'
        key_value 'Sample text'
    end
    factory :state_admin_keyword, parent: :admin_keyword do
        key_type 'State'
        key_subtype ''
        key_value 'ACT'
    end
    factory :invalid_key_type, parent: :admin_keyword  do
        key_type nil
    end
    factory :invalid_key_value, parent: :admin_keyword  do
        key_value nil
    end

end
