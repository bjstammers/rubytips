FactoryBot.define do
  factory :assoc_client, class: Client do
    client_name 'Sample Client'
    street_address '1 Test Avenue'
    street_city 'Brisbane'
    street_state 'QLD'
    street_postcode '4001'
    postal_address 'GPO Box 1234'
    postal_city 'Brisbane'
    postal_state 'QLD'
    postal_postcode '4000'
    contact_name 'Fred Smith'
    contact_phone '0411 222 333'
    contact_fax '(07) 3333 4444'
    contact_email 'fsmith@sample.com.au'
    client_tax 'Yes'
    client_super 'Yes'
    client_abn '111 22 3333'
    client_terms '7 days'
  end
end
