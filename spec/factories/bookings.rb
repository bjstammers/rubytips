FactoryBot.define do
    factory :booking_display do
        booking_ref 'ABC123'
        start_date 2018-01-01
        end_date 2018-01-05
        description "Some text"
        duration "4 days"

        association :client, factory: :assoc_client
        # association :booking_details, factory: :assoc_booking_details
    end

    factory :booking do
        booking_ref 'ABC123'
        start_date 2018-01-01
        end_date 2018-01-05
        description "Some text"
        duration "4 days"

        association :client, factory: :assoc_client
        association :booking_details, factory: :assoc_booking_details
    end

    factory :invalid_booking_ref, parent: :booking  do
        booking_ref nil
    end
    factory :invalid_start_date, parent: :booking  do
        start_date nil
    end
    factory :invalid_end_date, parent: :booking  do
        end_date nil
    end
    factory :invalid_description, parent: :booking  do
        description nil
    end
    factory :invalid_duration, parent: :booking  do
        duration nil
    end
end
