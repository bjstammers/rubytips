FactoryBot.define do
    factory :assoc_employee, class: Employee do
        first_name "Robert"
        middle_name "James"
        surname "Smith"
        full_name "Bob Smith"
        known_as "Bob"
        address "2 Test Avenue"
        city "Brisbane"
        state "QLD"
        postcode "4000"
        home_phone "07 3333 1111"
        work_phone "07 3333 2222"
        mobile_phone "0422 222 222"
        email "bsmith@example.com.au"
        birth_date 01-01-1979
        dob_display "Yes"
        superannuation_fund "SampleSuper"
        superannuation_no "ABC1234"
        tax_file_number "123 456 789"
        abn "9999 88 777"
        gst_registered "No"
    end
end
