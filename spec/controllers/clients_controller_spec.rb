require 'rails_helper'

RSpec.describe ClientsController, type: :controller do

  before :each do
    @client = create(:client)
  end

  describe "GET #index" do
    it "returns index of all clients" do
      get :index
      should render_template(:index)
    end
  end

  describe "GET #show" do
    it "returns show client" do
      get :show, params: {id: @client.id }
      should render_template(:show)
    end
  end

  describe "GET #new" do
    it "returns the new client form" do
      get :new
      should render_template(:new)
    end
  end

   describe "POST #create" do
     context "with valid attributes" do
       it "create a new client successfully" do
         expect {
           post :create, params: {client: attributes_for(:client) }
         }.to change{Client.count}.by(1)
       end
       it "opens the new client" do
         post :create, params: {client: attributes_for(:client)}
         should redirect_to(Client.last)
       end
     end
  
    context "with invalid attributes" do
      it "does not create a new client without client name" do
        expect {
          post :create, params: {client: FactoryBot.attributes_for(:invalid_client_name)}
        }.to_not change{Client.count}
      end
      it "re-opens the new client form for an invalid client name" do
        post :create, params: {client: FactoryBot.attributes_for(:invalid_client_name)}
        should render_template(:new)
      end
      it "does not create a new client without street_address" do
        expect {
          post :create, params: {client: FactoryBot.attributes_for(:invalid_street_address)}
        }.to_not change{Client.count}
      end
      it "re-opens the new client form for an invalid street_address" do
        post :create, params: {client: FactoryBot.attributes_for(:invalid_street_address)}
        should render_template(:new)
      end
      it "does not create a new client without street_city" do
        expect {
          post :create, params: {client: FactoryBot.attributes_for(:invalid_street_city)}
        }.to_not change{Client.count}
      end
      it "re-opens the new client form for an invalid street_city" do
        post :create, params: {client: FactoryBot.attributes_for(:invalid_street_city)}
        should render_template(:new)
      end
      it "does not create a new client without street_state" do
        expect {
          post :create, params: {client: FactoryBot.attributes_for(:invalid_street_state)}
        }.to_not change{Client.count}
      end
      it "re-opens the new client form for an invalid street_state" do
        post :create, params: {client: FactoryBot.attributes_for(:invalid_street_state)}
        should render_template(:new)
      end
      it "does not create a new client without street_postcode" do
        expect {
          post :create, params: {client: FactoryBot.attributes_for(:invalid_street_postcode)}
        }.to_not change{Client.count}
      end
      it "re-opens the new client form for an invalid street_postcode" do
        post :create, params: {client: FactoryBot.attributes_for(:invalid_street_postcode)}
        should render_template(:new)
      end
      it "does not create a new client without contact name" do
        expect {
          post :create, params: {client: FactoryBot.attributes_for(:invalid_contact_email)}
        }.to_not change{Client.count}
      end
      it "re-opens the new client form for an invalid contact name" do
        post :create, params: {client: FactoryBot.attributes_for(:invalid_contact_email)}
        should render_template(:new)
      end
      it "does not create a new client without contact email" do
        expect {
          post :create, params: {client: FactoryBot.attributes_for(:invalid_contact_email)}
        }.to_not change{Client.count}
      end
      it "re-opens the new client form for an invalid contact email" do
        post :create, params: {client: FactoryBot.attributes_for(:invalid_contact_email)}
        should render_template(:new)
      end
      it "does not create a new client without contact phone" do
        expect {
          post :create, params: {client: FactoryBot.attributes_for(:invalid_contact_phone)}
        }.to_not change{Client.count}
      end
      it "re-opens the new client form for an invalid contact phone" do
        post :create, params: {client: FactoryBot.attributes_for(:invalid_contact_phone)}
        should render_template(:new)
      end
      it "does not create a new client without abn" do
        expect {
          post :create, params: {client: FactoryBot.attributes_for(:invalid_client_abn)}
        }.to_not change{Client.count}
      end
      it "re-opens the new client form for an invalid abn" do
        post :create, params: {client: FactoryBot.attributes_for(:invalid_client_abn)}
        should render_template(:new)
      end
    end
   end
  
  describe "GET #edit" do
    it "should open the client edit form" do
      get :edit, params: {id: @client.id }
      should render_template(:edit)
    end
  end
  
  describe "PUT #update" do
    context "with valid attributess" do
      before :each do
        put :update, params: {id: @client, client: FactoryBot.attributes_for(:client, client_name: "Joe Bloggs Ltd", street_address: "23 Thomas Street", street_city: "Chermside", street_state: "QLD", street_postcode: "4032", contact_name: "Fred Smith", contact_email: "jb@gmail.com", contact_phone: "55512222", client_abn: "123456789")}
        @client.reload
      end
      it "should update the client_name" do
        @client.client_name.should eq("Joe Bloggs Ltd")
      end
      it "should update the street_address" do
        @client.street_address.should eq("23 Thomas Street")
      end
      it "should update the street_city" do
        @client.street_city.should eq("Chermside")
      end
      it "should update the street_state" do
        @client.street_state.should eq("QLD")
      end
      it "should update the street_postcode" do
        @client.street_postcode.should eq("4032")
      end
      it "should update the contact_name" do
        @client.contact_name.should eq("Fred Smith")
      end
      it "should update the contact_email" do
        @client.contact_email.should eq("jb@gmail.com")
      end
      it "should update the contact_phone" do
        @client.contact_phone.should eq("55512222")
      end
      it "should update the client_abn" do
        @client.client_abn.should eq("123456789")
      end
      it "opens the updated client" do
        put :update, params: {id: @client, client: FactoryBot.attributes_for(:client, client_name: "Joe Bloggs Ltd", street_address: "23 Thomas Street", street_city: "Chermside", street_state: "QLD", street_postcode: "4032", contact_name: "Fred Smith", contact_email: "jb@gmail.com", contact_phone: "55512222", client_abn: "123456789")}
        should redirect_to(@client)
      end
    end
  
    context "with invalid attributes" do
      before :each do
        put :update, params: {id: @client, client: FactoryBot.attributes_for(:client, client_name: nil, street_address: nil, street_city: nil, street_state: nil, street_postcode: nil, postal_address: "PO Box 1234", contact_name: nil, contact_email: nil, contact_phone: nil, client_abn: nil)}
        @client.reload
      end
      it "should not update the client_name" do
        @client.client_name.should eq("Sample Client")
      end
      it "should not update the street_address" do
        @client.street_address.should eq("1 Test Avenue")
      end
      it "should not update the street_city" do
        @client.street_city.should eq("Brisbane")
      end
      it "should not update the street_state" do
        @client.street_state.should eq("QLD")
      end
      it "should not update the street_postcode" do
        @client.street_postcode.should eq("4001")
      end
      it "should not update the postal_address" do
        @client.postal_address.should eq("GPO Box 1234")
      end
      it "should not update the contact_name" do
        @client.contact_name.should eq("Fred Smith")
      end
      it "should not update the contact_email" do
        @client.contact_email.should eq("fsmith@sample.com.au")
      end
      it "should not update the contact_phone" do
        @client.contact_phone.should eq("0411 222 333")
      end
      it "should not update the client_abn" do
        @client.client_abn.should eq("111 22 3333")
      end
      it "re-opens the edit page" do
        put :update, params: {id: @client, client: FactoryBot.attributes_for(:client, client_name: nil, street_address: nil, street_city: nil, street_state: nil, street_postcode: nil, postal_address: "PO Box 1234", contact_name: nil, contact_email: nil, contact_phone: nil, client_abn: nil)}
        should render_template(:edit)
      end
    end
  end
  
  describe "DELETE #destroy" do
    it "deletes the client" do
      @client = FactoryBot.create(:client)
      expect {
        delete :destroy, params: { id: @client.id }
      }.to change{Client.count}.by(-1)
    end
    it "opens the client index page" do
      @client = FactoryBot.create(:client)
      delete :destroy, params: { id: @client.id }
      should redirect_to clients_url
    end
  end

end
