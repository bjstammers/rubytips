require 'rails_helper'

RSpec.describe BookingsController, type: :controller do

   before :each do
     @booking = create(:booking)
     @detail_attributes = FactoryBot.attributes_for(:booking_details, booking_id: @booking.id)
   end

  describe "GET #index" do
    it "returns index of all bookings" do
      get :index
      should render_template(:index)
    end
  end

  describe "GET #show" do
    it "returns show booking" do
      @booking = create(:booking_display)
      get :show, params: {id: @booking.id }
      should render_template(:show)
    end
  end

  describe "GET #new" do
    it "returns the new booking form" do
      get :new
      should render_template(:new)
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "create a new booking successfully" do
        # @booking = create(:booking)
        expect {
          post :create, params: {booking: attributes_for(:booking) }
        }.to change{Booking.count}.by(1)
      end
      it "opens the new booking" do
        post :create, params: {booking: attributes_for(:booking)}
        should redirect_to(Booking.last)
      end
    end

   # context "with invalid attributes" do
   #   it "does not create a new client without client name" do
   #     expect {
   #       post :create, params: {client: FactoryBot.attributes_for(:invalid_client_name)}
   #     }.to_not change{Client.count}
   #   end
   #   it "re-opens the new client form for an invalid client name" do
   #     post :create, params: {client: FactoryBot.attributes_for(:invalid_client_name)}
   #     should render_template(:new)
   #   end
   #   it "does not create a new client without street_address" do
   #     expect {
   #       post :create, params: {client: FactoryBot.attributes_for(:invalid_street_address)}
   #     }.to_not change{Client.count}
   #   end
   #   it "re-opens the new client form for an invalid street_address" do
   #     post :create, params: {client: FactoryBot.attributes_for(:invalid_street_address)}
   #     should render_template(:new)
   #   end
   #   it "does not create a new client without street_city" do
   #     expect {
   #       post :create, params: {client: FactoryBot.attributes_for(:invalid_street_city)}
   #     }.to_not change{Client.count}
   #   end
   #   it "re-opens the new client form for an invalid street_city" do
   #     post :create, params: {client: FactoryBot.attributes_for(:invalid_street_city)}
   #     should render_template(:new)
   #   end
   #   it "does not create a new client without street_state" do
   #     expect {
   #       post :create, params: {client: FactoryBot.attributes_for(:invalid_street_state)}
   #     }.to_not change{Client.count}
   #   end
   #   it "re-opens the new client form for an invalid street_state" do
   #     post :create, params: {client: FactoryBot.attributes_for(:invalid_street_state)}
   #     should render_template(:new)
   #   end
   #   it "does not create a new client without street_postcode" do
   #     expect {
   #       post :create, params: {client: FactoryBot.attributes_for(:invalid_street_postcode)}
   #     }.to_not change{Client.count}
   #   end
   #   it "re-opens the new client form for an invalid street_postcode" do
   #     post :create, params: {client: FactoryBot.attributes_for(:invalid_street_postcode)}
   #     should render_template(:new)
   #   end
   #   it "does not create a new client without contact name" do
   #     expect {
   #       post :create, params: {client: FactoryBot.attributes_for(:invalid_contact_email)}
   #     }.to_not change{Client.count}
   #   end
   #   it "re-opens the new client form for an invalid contact name" do
   #     post :create, params: {client: FactoryBot.attributes_for(:invalid_contact_email)}
   #     should render_template(:new)
   #   end
   #   it "does not create a new client without contact email" do
   #     expect {
   #       post :create, params: {client: FactoryBot.attributes_for(:invalid_contact_email)}
   #     }.to_not change{Client.count}
   #   end
   #   it "re-opens the new client form for an invalid contact email" do
   #     post :create, params: {client: FactoryBot.attributes_for(:invalid_contact_email)}
   #     should render_template(:new)
   #   end
   #   it "does not create a new client without contact phone" do
   #     expect {
   #       post :create, params: {client: FactoryBot.attributes_for(:invalid_contact_phone)}
   #     }.to_not change{Client.count}
   #   end
   #   it "re-opens the new client form for an invalid contact phone" do
   #     post :create, params: {client: FactoryBot.attributes_for(:invalid_contact_phone)}
   #     should render_template(:new)
   #   end
   #   it "does not create a new client without abn" do
   #     expect {
   #       post :create, params: {client: FactoryBot.attributes_for(:invalid_client_abn)}
   #     }.to_not change{Client.count}
   #   end
   #   it "re-opens the new client form for an invalid abn" do
   #     post :create, params: {client: FactoryBot.attributes_for(:invalid_client_abn)}
   #     should render_template(:new)
   #   end
   # end
  end


end
