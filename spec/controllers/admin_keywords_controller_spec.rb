require 'rails_helper'

describe AdminKeywordsController do

  before :each do
    @keyword = create(:admin_keyword)
  end

  describe "GET #index" do
    it "returns index of all keywords" do
      get :index
      should render_template(:index)
    end
  end

  describe "GET #show" do
    it "returns show keyword" do
      get :show, params: {id: @keyword.id }
      should render_template(:show)
    end
  end

  describe "GET #new" do
    it "returns the new keyword form" do
      get :new
      should render_template(:new)
    end
  end

   describe "POST #create" do
     context "with valid attributes" do
       it "create a new keyword successfully" do
         expect {
           post :create, params: {admin_keyword: attributes_for(:admin_keyword) }
         }.to change{AdminKeyword.count}.by(1)
       end
       it "opens the new keyword" do
         post :create, params: {admin_keyword: attributes_for(:admin_keyword)}
         should redirect_to(AdminKeyword.last)
       end
     end
  
    context "with invalid attributes" do
      it "does not create a new keyword without key_type" do
        expect {
          post :create, params: {admin_keyword: FactoryBot.attributes_for(:invalid_key_type)}
        }.to_not change{AdminKeyword.count}
      end
      it "re-opens the new keyword form for an invalid key_type" do
        post :create, params: {admin_keyword: FactoryBot.attributes_for(:invalid_key_type)}
        should render_template(:new)
      end
      it "does not create a new keyword without key_value" do
        expect {
          post :create, params: {admin_keyword: FactoryBot.attributes_for(:invalid_key_value)}
        }.to_not change{AdminKeyword.count}
      end
      it "re-opens the new keyword form for an invalid key_value" do
        post :create, params: {admin_keyword: FactoryBot.attributes_for(:invalid_key_value)}
        should render_template(:new)
      end
    end
   end
  
  describe "GET #edit" do
    it "should open the keyword edit form" do
      get :edit, params: {id: @keyword.id }
      should render_template(:edit)
    end
  end
  
  describe "PUT #update" do
    context "with valid attributes" do
      before :each do
        @test_keyword = create(:admin_keyword)
        put :update, params: {id: @test_keyword, admin_keyword: FactoryBot.attributes_for(:admin_keyword, key_type: "Example 1", key_subtype: "Example 2", key_value: "Just more text")}
        @test_keyword.reload
      end
      it "should update the key_type" do
        @test_keyword.key_type.should eq("Example 1")
      end
      it "should update the key_subtype" do
        @test_keyword.key_subtype.should eq("Example 2")
      end
      it "should update the street_city" do
        @test_keyword.key_value.should eq("Just more text")
      end
      it "opens the updated keyword" do
        put :update, params: {id: @test_keyword, admin_keyword: FactoryBot.attributes_for(:admin_keyword, key_type: "Example 1", key_subtype: "Example 2", key_value: "Just more text")}
        should redirect_to(@test_keyword)
      end
    end
  
    context "with invalid attributes" do
      before :each do
        @test_keyword = create(:admin_keyword)
        put :update, params: {id: @test_keyword, admin_keyword: FactoryBot.attributes_for(:admin_keyword, key_type: nil, key_subtype: "Example 2", key_value: nil)}
        @test_keyword.reload
      end
      it "should not update the key_type" do
        @test_keyword.key_type.should eq(@keyword.key_type)
      end
      it "should not update the key_subtype" do
        @test_keyword.key_subtype.should eq(@keyword.key_subtype)
      end
      it "should not update the key_value" do
        @test_keyword.key_value.should eq(@keyword.key_value)
      end
      it "re-opens the edit page" do
        put :update, params: {id: @test_keyword, admin_keyword: FactoryBot.attributes_for(:admin_keyword, key_type: nil, key_subtype: "Example 2", key_value: nil)}
        should render_template(:edit)
      end
    end
  end
  
  describe "DELETE #destroy" do
    it "deletes the keyword" do
      @keyword = FactoryBot.create(:admin_keyword)
      expect {
        delete :destroy, params: { id: @keyword.id }
      }.to change{AdminKeyword.count}.by(-1)
    end
    it "opens the keyword index page" do
      @keyword = FactoryBot.create(:admin_keyword)
      delete :destroy, params: { id: @keyword.id }
      should redirect_to admin_keywords_url
    end
  end


end
