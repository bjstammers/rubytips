require 'rails_helper'

RSpec.describe EmployeesController, type: :controller do
  before :each do
    @employee = create(:employee)
  end

  describe "GET #index" do
    it "returns index of all employees" do
      get :index
      should render_template(:index)
    end
  end

  describe "GET #show" do
    it "returns show employee" do
      get :show, params: {id: @employee.id }
      should render_template(:show)
    end
  end

  describe "GET #new" do
    it "returns the new employee form" do
      get :new
      should render_template(:new)
    end
  end

   describe "POST #create" do
     context "with valid attributes" do
       it "create a new employee successfully" do
         expect {
           post :create, params: {employee: attributes_for(:employee) }
         }.to change{Employee.count}.by(1)
       end
       it "opens the new employee" do
         post :create, params: {employee: attributes_for(:employee)}
         should redirect_to(Employee.last)
       end
     end
    context "with invalid attributes" do
      it "does not create a new employee without first name" do
        expect {
          post :create, params: {employee: FactoryBot.attributes_for(:invalid_first_name)}
        }.to_not change{Employee.count}
      end
      it "re-opens the new employee form for an invalid first name" do
        post :create, params: {employee: FactoryBot.attributes_for(:invalid_first_name)}
        should render_template(:new)
      end
      it "does not create a new employee without surname" do
        expect {
          post :create, params: {employee: FactoryBot.attributes_for(:invalid_surname)}
        }.to_not change{Employee.count}
      end
      it "re-opens the new employee form for an invalid surname" do
        post :create, params: {employee: FactoryBot.attributes_for(:invalid_surname)}
        should render_template(:new)
      end
      it "does not create a new employee without full name" do
        expect {
          post :create, params: {employee: FactoryBot.attributes_for(:invalid_full_name)}
        }.to_not change{Employee.count}
      end
      it "re-opens the new employee form for an invalid full name" do
        post :create, params: {employee: FactoryBot.attributes_for(:invalid_full_name)}
        should render_template(:new)
      end
      it "does not create a new employee without address" do
        expect {
          post :create, params: {employee: FactoryBot.attributes_for(:invalid_address)}
        }.to_not change{Employee.count}
      end
      it "re-opens the new employee form for an invalid address" do
        post :create, params: {employee: FactoryBot.attributes_for(:invalid_address)}
        should render_template(:new)
      end
      it "does not create a new employee without city" do
        expect {
          post :create, params: {employee: FactoryBot.attributes_for(:invalid_city)}
        }.to_not change{Employee.count}
      end
      it "re-opens the new employee form for an invalid city" do
        post :create, params: {employee: FactoryBot.attributes_for(:invalid_city)}
        should render_template(:new)
      end
      it "does not create a new employee without state" do
        expect {
          post :create, params: {employee: FactoryBot.attributes_for(:invalid_state)}
        }.to_not change{Employee.count}
      end
      it "re-opens the new employee form for an invalid state" do
        post :create, params: {employee: FactoryBot.attributes_for(:invalid_state)}
        should render_template(:new)
      end
      it "does not create a new employee without postcode" do
        expect {
          post :create, params: {employee: FactoryBot.attributes_for(:invalid_postcode)}
        }.to_not change{Employee.count}
      end
      it "re-opens the new employee form for an invalid postcode" do
        post :create, params: {employee: FactoryBot.attributes_for(:invalid_postcode)}
        should render_template(:new)
      end
      it "does not create a new employee without home phone" do
        expect {
          post :create, params: {employee: FactoryBot.attributes_for(:invalid_home_phone)}
        }.to_not change{Employee.count}
      end
      it "re-opens the new employee form for an invalid home phone" do
        post :create, params: {employee: FactoryBot.attributes_for(:invalid_home_phone)}
        should render_template(:new)
      end
      it "does not create a new employee without mobile phone" do
        expect {
          post :create, params: {employee: FactoryBot.attributes_for(:invalid_mobile_phone)}
        }.to_not change{Employee.count}
      end
      it "re-opens the new employee form for an invalid mobile phone" do
        post :create, params: {employee: FactoryBot.attributes_for(:invalid_mobile_phone)}
        should render_template(:new)
      end
    end
   end
  
  describe "GET #edit" do
    it "should open the employee edit form" do
      get :edit, params: {id: @employee.id }
      should render_template(:edit)
    end
  end
  
  describe "PUT #update" do
    context "with valid attributes" do
      before :each do
  #validates :first_name, :surname, :full_name, :address, :city, :postcode, :state, :home_phone, :mobile_phone, presence: true
        put :update, params: {id: @employee, employee: FactoryBot.attributes_for(:employee, first_name: "Joe", surname: "Bloggs", full_name: "Joe Bloggs", address: "99 Example Road", city: "Sydney", state: "NSW", postcode: "2010", home_phone: "(02) 9999 1111", mobile_phone: "0433 111 222")}
        @employee.reload
      end
      it "should update the first_name" do
        @employee.first_name.should eq("Joe")
      end
      it "should update the surname" do
        @employee.surname.should eq("Bloggs")
      end
      it "should update the full_name" do
        @employee.full_name.should eq("Joe Bloggs")
      end
      it "should update the address" do
        @employee.address.should eq("99 Example Road")
      end
      it "should update the city" do
        @employee.city.should eq("Sydney")
      end
      it "should update the state" do
        @employee.state.should eq("NSW")
      end
      it "should update the postcode" do
        @employee.postcode.should eq("2010")
      end
      it "should update the home phone" do
        @employee.home_phone.should eq("(02) 9999 1111")
      end
      it "should update the mobile phone" do
        @employee.mobile_phone.should eq("0433 111 222")
      end
      it "opens the updated employee" do
        put :update, params: {id: @employee, employee: FactoryBot.attributes_for(:employee, first_name: "Joe", surname: "Bloggs", full_name: "Joe Bloggs", address: "99 Example Road", city: "Sydney", state: "NSW", postcode: "2010", home_phone: "(02) 9999 1111", mobile_phone: "0433 111 222")}
        should redirect_to(@employee)
      end
    end
  
    context "with invalid attributes" do
      before :each do
        put :update, params: {id: @employee, employee: FactoryBot.attributes_for(:employee, first_name: nil, surname: nil, full_name: nil, address: nil, city: nil, state: nil, postcode: nil, home_phone: nil, mobile_phone: nil, known_as: "Joseph")}
        @employee.reload
      end
      it "should not update the first name" do
        @employee.first_name.should eq("Robert")
      end
      it "should not update the street_address" do
        @employee.surname.should eq("Smith")
      end
      it "should not update the full_name" do
        @employee.full_name.should eq("Bob Smith")
      end
      it "should not update the address" do
        @employee.address.should eq("2 Test Avenue")
      end
      it "should not update the city" do
        @employee.city.should eq("Brisbane")
      end
      it "should not update the state" do
        @employee.state.should eq("QLD")
      end
      it "should not update the postcode" do
        @employee.postcode.should eq("4000")
      end
      it "should not update the home phone" do
        @employee.home_phone.should eq("07 3333 1111")
      end
      it "should not update the mobile phone" do
        @employee.mobile_phone.should eq("0422 222 222")
      end
      it "should not update the known as" do
        @employee.known_as.should eq("Bob")
      end
      it "re-opens the edit page" do
        put :update, params: {id: @employee, employee: FactoryBot.attributes_for(:employee, first_name: nil, surname: nil, full_name: nil, address: nil, city: nil, state: nil, postcode: nil, home_phone: nil, mobile_phone: nil, known_as: "Joseph")}
        should render_template(:edit)
      end
    end
  end
  
  describe "DELETE #destroy" do
    it "deletes the employee" do
      @employee = FactoryBot.create(:employee)
      expect {
        delete :destroy, params: { id: @employee.id }
      }.to change{Employee.count}.by(-1)
    end
    it "opens the employee index page" do
      @employee = FactoryBot.create(:employee)
      delete :destroy, params: { id: @employee.id }
      should redirect_to employees_url
    end
  end

end
