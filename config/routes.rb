Rails.application.routes.draw do
  resources :invoices do
    
  end
  resources :bookings do
    get 'search', on: :member
    resources :booking_details
  end

  get 'static_pages/welcome'
  get 'static_pages/help'

  root 'static_pages#welcome'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

#   resources :clients, shallow: :true do
#     resources :invoices do
#       resources :invoice_details
#       resources :invoice_payments do
#         resources :invoice_payment_details
#       end
#     end
#   end

#   resources :people, shallow: :true do
#     resources :payments do
#       resources :payment_details
#       resources :payment_extras
#     end
#   end
    resources :admin_keywords

    resources :clients, shallow: :true do
        # resources :invoices do

        # end
        resources :bookings

    end

    resources :employees, shallow: :true do

    end

end
