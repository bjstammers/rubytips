require 'test_helper'

class EmployeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @employee = employees(:one)
  end

  test "should get index" do
    get employees_url
    assert_response :success
  end

  test "should get new" do
    get new_employee_url
    assert_response :success
  end

  test "should create employee" do
    assert_difference('Employee.count') do
      post employees_url, params: { employee: { abn: @employee.abn, address: @employee.address, birth_date: @employee.birth_date, city: @employee.city, dob_display: @employee.dob_display, email: @employee.email, first_name: @employee.first_name, full_name: @employee.full_name, gst_registered: @employee.gst_registered, home_phone: @employee.home_phone, known_as: @employee.known_as, middle_name: @employee.middle_name, mobile_phone: @employee.mobile_phone, postcode: @employee.postcode, state: @employee.state, superannuation_fund: @employee.superannuation_fund, superannuation_no: @employee.superannuation_no, surname: @employee.surname, tax_file_number: @employee.tax_file_number, work_phone: @employee.work_phone } }
    end

    assert_redirected_to employee_url(Employee.last)
  end

  test "should show employee" do
    get employee_url(@employee)
    assert_response :success
  end

  test "should get edit" do
    get edit_employee_url(@employee)
    assert_response :success
  end

  test "should update employee" do
    patch employee_url(@employee), params: { employee: { abn: @employee.abn, address: @employee.address, birth_date: @employee.birth_date, city: @employee.city, dob_display: @employee.dob_display, email: @employee.email, first_name: @employee.first_name, full_name: @employee.full_name, gst_registered: @employee.gst_registered, home_phone: @employee.home_phone, known_as: @employee.known_as, middle_name: @employee.middle_name, mobile_phone: @employee.mobile_phone, postcode: @employee.postcode, state: @employee.state, superannuation_fund: @employee.superannuation_fund, superannuation_no: @employee.superannuation_no, surname: @employee.surname, tax_file_number: @employee.tax_file_number, work_phone: @employee.work_phone } }
    assert_redirected_to employee_url(@employee)
  end

  test "should destroy employee" do
    assert_difference('Employee.count', -1) do
      delete employee_url(@employee)
    end

    assert_redirected_to employees_url
  end
end
