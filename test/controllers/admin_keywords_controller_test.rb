require 'test_helper'

class AdminKeywordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_keyword = admin_keywords(:one)
  end

  test "should get index" do
    get admin_keywords_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_keyword_url
    assert_response :success
  end

  test "should create admin_keyword" do
    assert_difference('AdminKeyword.count') do
      post admin_keywords_url, params: { admin_keyword: { subtype: @admin_keyword.subtype, type: @admin_keyword.type, value: @admin_keyword.value } }
    end

    assert_redirected_to admin_keyword_url(AdminKeyword.last)
  end

  test "should show admin_keyword" do
    get admin_keyword_url(@admin_keyword)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_keyword_url(@admin_keyword)
    assert_response :success
  end

  test "should update admin_keyword" do
    patch admin_keyword_url(@admin_keyword), params: { admin_keyword: { subtype: @admin_keyword.subtype, type: @admin_keyword.type, value: @admin_keyword.value } }
    assert_redirected_to admin_keyword_url(@admin_keyword)
  end

  test "should destroy admin_keyword" do
    assert_difference('AdminKeyword.count', -1) do
      delete admin_keyword_url(@admin_keyword)
    end

    assert_redirected_to admin_keywords_url
  end
end
