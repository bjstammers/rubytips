require 'test_helper'

class InvoicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @invoice = invoices(:one)
  end

  test "should get index" do
    get invoices_url
    assert_response :success
  end

  test "should get new" do
    get new_invoice_url
    assert_response :success
  end

  test "should create invoice" do
    assert_difference('Invoice.count') do
      post invoices_url, params: { invoice: { booking_id: @invoice.booking_id, client_id: @invoice.client_id, current_gross: @invoice.current_gross, current_gst: @invoice.current_gst, gst_included: @invoice.gst_included, gst_invoice: @invoice.gst_invoice, gst_service_fee: @invoice.gst_service_fee, invoice_currency: @invoice.invoice_currency, invoice_date: @invoice.invoice_date, invoice_ref: @invoice.invoice_ref, invoice_total: @invoice.invoice_total, paid_flag: @invoice.paid_flag, print_flag: @invoice.print_flag, service_fee: @invoice.service_fee, super_total: @invoice.super_total } }
    end

    assert_redirected_to invoice_url(Invoice.last)
  end

  test "should show invoice" do
    get invoice_url(@invoice)
    assert_response :success
  end

  test "should get edit" do
    get edit_invoice_url(@invoice)
    assert_response :success
  end

  test "should update invoice" do
    patch invoice_url(@invoice), params: { invoice: { booking_id: @invoice.booking_id, client_id: @invoice.client_id, current_gross: @invoice.current_gross, current_gst: @invoice.current_gst, gst_included: @invoice.gst_included, gst_invoice: @invoice.gst_invoice, gst_service_fee: @invoice.gst_service_fee, invoice_currency: @invoice.invoice_currency, invoice_date: @invoice.invoice_date, invoice_ref: @invoice.invoice_ref, invoice_total: @invoice.invoice_total, paid_flag: @invoice.paid_flag, print_flag: @invoice.print_flag, service_fee: @invoice.service_fee, super_total: @invoice.super_total } }
    assert_redirected_to invoice_url(@invoice)
  end

  test "should destroy invoice" do
    assert_difference('Invoice.count', -1) do
      delete invoice_url(@invoice)
    end

    assert_redirected_to invoices_url
  end
end
