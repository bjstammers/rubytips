from django.contrib.auth.decorators import login_required
from django.db import models
from django.utils import timezone

States = (('ACT', 'ACT'),('NSW', 'NSW'),('NT', 'NT'),('QLD', 'QLD'),('SA', 'SA'),('TAS', 'TAS'),('VIC', 'VIC'),('WA', 'WA'),)
YN = (('Yes', 'Yes'), ('No', 'No'))

class Client(models.Model):    
    client_name = models.CharField(max_length=200)
    street_address = models.CharField(max_length=200)
    street_city = models.CharField(max_length=200)
    street_state = models.CharField(max_length=3, choices=States, blank=False, default='ACT')
    street_postcode = models.CharField(max_length=200)
    postal_address = models.CharField(max_length=200)
    postal_city = models.CharField(max_length=200)
    postal_state = models.CharField(max_length=3, choices=States, blank=False, default='ACT')
    postal_postcode = models.CharField(max_length=200)
    contact_name = models.CharField(max_length=200)
    contact_phone = models.CharField(max_length=200)
    contact_fax = models.CharField(max_length=200)
    contact_email = models.CharField(max_length=200)
    client_tax = models.CharField(max_length=200, choices=YN, blank=False, default='No')
    client_super = models.CharField(max_length=200, choices=YN, blank=False, default='No')
    client_abn = models.CharField(max_length=200)
    client_terms = models.TextField()
    
    def display_address(self):
        return self.street_address + ", " + self.street_city + ", " + self.street_state + " " + self.street_postcode
    
class Test(models.Model):
    field01 = models.CharField(max_length=200)
    field02 = models.CharField(max_length=200)
    
class Person(models.Model):
    first_name = models.CharField(max_length=200)
    middle_name = models.CharField(max_length=200)
    surname = models.CharField(max_length=200)
    full_name = models.CharField(max_length=200)
    known_as = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=3, choices=States, blank=False, default='ACT')
    postcode = models.CharField(max_length=200)
    home_phone = models.CharField(max_length=200)
    work_phone = models.CharField(max_length=200)
    mobile_phone = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    birth_date = models.DateField()
    dob_display = models.CharField(max_length=200, choices=YN, blank=False, default='No')
    superannuation_fund = models.CharField(max_length=200)
    superannuation_no = models.CharField(max_length=200)
    tax_file_number = models.CharField(max_length=200)
    abn = models.CharField(max_length=200)
    gst_registered = models.CharField(max_length=200, choices=YN, blank=False, default='No')
    #current_yearly_earnings = models.DecimalField(max_digits=6, decimal_places=2)
    #current_yearly_payments = models.DecimalField(max_digits=6, decimal_places=2)

    def display_address(self):
        return self.address + ", " + self.city + ", " + self.state + " " + self.postcode
    
    def display_name(self):
        if self.known_as == "":
            return self.first_name + " " + self.surname
        else:
            return self.known_as + " " + self.surname
    
    
class Booking(models.Model):
    booking_ref = models.CharField(max_length=200)
    booking_start_date = models.DateField()
    booking_end_date = models.DateField()
    booking_details = models.CharField(max_length=200)
    booking_duration = models.CharField(max_length=200)
    client_id = models.ForeignKey('Client', on_delete=models.PROTECT)
    
    

class BookingDetail(models.Model):
    person_id = models.ForeignKey('Person', on_delete=models.PROTECT)
    start_date = models.DateField()
    end_date = models.DateField()
    duration = models.CharField(max_length=200)
    booking_id = models.ForeignKey('Booking', on_delete=models.CASCADE)

    
    

