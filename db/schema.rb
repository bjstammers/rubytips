# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180508103300) do

  create_table "admin_keywords", force: :cascade do |t|
    t.string "key_type"
    t.string "key_subtype"
    t.string "key_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "booking_details", force: :cascade do |t|
    t.date "start_date"
    t.date "end_date"
    t.string "duration"
    t.string "notes"
    t.integer "booking_id"
    t.integer "employee_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["booking_id"], name: "index_booking_details_on_booking_id"
    t.index ["employee_id"], name: "index_booking_details_on_employee_id"
  end

  create_table "bookings", force: :cascade do |t|
    t.string "booking_ref"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "start_date"
    t.date "end_date"
    t.string "duration"
    t.string "description"
    t.integer "client_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string "client_name"
    t.string "street_address"
    t.string "street_city"
    t.string "street_state"
    t.string "street_postcode"
    t.string "postal_address"
    t.string "postal_city"
    t.string "postal_state"
    t.string "postal_postcode"
    t.string "contact_name"
    t.string "contact_phone"
    t.string "contact_fax"
    t.string "contact_email"
    t.boolean "client_tax"
    t.boolean "client_super"
    t.string "client_abn"
    t.string "client_terms"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string "first_name"
    t.string "middle_name"
    t.string "surname"
    t.string "full_name"
    t.string "known_as"
    t.string "address"
    t.string "city"
    t.string "state"
    t.string "postcode"
    t.string "home_phone"
    t.string "work_phone"
    t.string "mobile_phone"
    t.string "email"
    t.date "birth_date"
    t.boolean "dob_display"
    t.string "superannuation_fund"
    t.string "superannuation_no"
    t.string "tax_file_number"
    t.string "abn"
    t.boolean "gst_registered"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hotels", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invoice_details", force: :cascade do |t|
    t.integer "invoice_id"
    t.float "gross_total"
    t.float "employee_total"
    t.float "commission"
    t.float "taxable_amount"
    t.float "gst_employee"
    t.float "super"
    t.float "tax"
    t.float "commission_gst"
    t.boolean "paid_flag"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "employee_id"
    t.index ["invoice_id"], name: "index_invoice_details_on_invoice_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.string "invoice_ref"
    t.date "invoice_date"
    t.integer "client_id"
    t.integer "booking_id"
    t.float "super_total"
    t.float "service_fee"
    t.boolean "gst_included"
    t.float "gst_service_fee"
    t.float "gst_invoice"
    t.string "invoice_currency"
    t.float "invoice_total"
    t.boolean "print_flag"
    t.boolean "paid_flag"
    t.float "current_gross"
    t.float "current_gst"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["booking_id"], name: "index_invoices_on_booking_id"
    t.index ["client_id"], name: "index_invoices_on_client_id"
  end

  create_table "room_categories", force: :cascade do |t|
    t.string "name"
    t.integer "hotel_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hotel_id"], name: "index_room_categories_on_hotel_id"
  end

end
