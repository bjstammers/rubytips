class AddExtraFields < ActiveRecord::Migration[5.1]
  def change
    add_column :bookings, :start_date, :date
    add_column :bookings, :end_date, :date
    add_column :bookings, :duration, :string
    add_column :bookings, :description, :string
    add_column :b_details, :start_date, :date
    add_column :b_details, :end_date, :date
    add_column :b_details, :duration, :string
  end
end
