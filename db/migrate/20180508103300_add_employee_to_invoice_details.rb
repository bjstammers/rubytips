class AddEmployeeToInvoiceDetails < ActiveRecord::Migration[5.1]
  def change
    add_column :invoice_details, :employee_id, :integer
  end
end
