class ConvertDobToDate < ActiveRecord::Migration[5.1]
  def change
    change_column :employees, :birth_date, :date
  end
end
