class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :first_name
      t.string :middle_name
      t.string :surname
      t.string :full_name
      t.string :known_as
      t.string :address
      t.string :city
      t.string :state
      t.string :postcode
      t.string :home_phone
      t.string :work_phone
      t.string :mobile_phone
      t.string :email
      t.datetime :birth_date
      t.string :dob_display
      t.string :superannuation_fund
      t.string :superannuation_no
      t.string :tax_file_number
      t.string :abn
      t.string :gst_registered

      t.timestamps
    end
  end
end
