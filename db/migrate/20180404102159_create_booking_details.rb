class CreateBookingDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :booking_details do |t|
      t.date :start_date
      t.date :end_date
      t.string :duration
      t.string :notes
      t.references :booking, foreign_key: true
      t.references :employee, foreign_key: true

      t.timestamps
    end
  end
end
