class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      t.string :client_name
      t.string :street_address
      t.string :street_city
      t.string :street_state
      t.string :street_postcode
      t.string :postal_address
      t.string :postal_city
      t.string :postal_state
      t.string :postal_postcode
      t.string :contact_name
      t.string :contact_phone
      t.string :contact_fax
      t.string :contact_email
      t.string :client_tax
      t.string :client_super
      t.string :client_abn
      t.string :client_terms

      t.timestamps
    end
  end
end
