class CreateAdminKeywords < ActiveRecord::Migration[5.1]
  def change
    create_table :admin_keywords do |t|
      t.string :type
      t.string :subtype
      t.string :value

      t.timestamps
    end
  end
end
