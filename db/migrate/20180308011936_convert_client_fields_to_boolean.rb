class ConvertClientFieldsToBoolean < ActiveRecord::Migration[5.1]
  def change
    change_column :clients, :client_tax, :boolean
    change_column :clients, :client_super, :boolean
    change_column :employees, :dob_display, :boolean
    change_column :employees, :gst_registered, :boolean
  end
end
