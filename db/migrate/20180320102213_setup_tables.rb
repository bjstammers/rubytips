class SetupTables < ActiveRecord::Migration[5.1]
  def change
    create_table :bookings do |t|
      t.string :booking_ref

      t.timestamps
    end
    create_table :b_details do |t|
        t.string :notes
        t.references :booking, foreign_key: true

        t.timestamps
    end
  end
end
