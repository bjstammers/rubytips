class FixKeywordColumns < ActiveRecord::Migration[5.1]
  def change
    rename_column :admin_keywords, :type, :key_type
    rename_column :admin_keywords, :subtype, :key_subtype
    rename_column :admin_keywords, :value, :key_value
  end
end
