class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.string :invoice_ref
      t.date :invoice_date
      t.references :client, foreign_key: true
      t.references :booking, foreign_key: true
      t.float :super_total
      t.float :service_fee
      t.boolean :gst_included
      t.float :gst_service_fee
      t.float :gst_invoice
      t.string :invoice_currency
      t.float :invoice_total
      t.boolean :print_flag
      t.boolean :paid_flag
      t.float :current_gross
      t.float :current_gst

      t.timestamps
    end
  end
end
