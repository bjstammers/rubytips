class CreateInvoiceDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :invoice_details do |t|
      t.references :invoice, foreign_key: true
      t.float :gross_total
      t.float :employee_total
      t.float :commission
      t.float :taxable_amount
      t.float :gst_employee
      t.float :super
      t.float :tax
      t.float :commission_gst
      t.boolean :paid_flag

      t.timestamps
    end
  end
end
