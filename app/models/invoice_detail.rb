class InvoiceDetail < ApplicationRecord
  validates :invoice_id, :gross_total, :employee_total, presence: true

  belongs_to :invoice
  belongs_to :employee

  
end
