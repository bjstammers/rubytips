class BookingDetail < ApplicationRecord
  validates :start_date, :end_date, :notes, :duration, :employee_id, presence: true
  belongs_to :booking
  belongs_to :employee

  def employee_name
    if self.employee_id != "" then
      @employee = Employee.find(self.employee_id)
      return @employee.display_name
    end
  end
end
