class Booking < ApplicationRecord
  validates :booking_ref, :start_date, :end_date, :description, :duration, :client_id, presence: true
  
  has_many :booking_details, dependent: :destroy
  belongs_to :client

  accepts_nested_attributes_for :booking_details

  def client_name
    if self.client_id != "" then
      return Client.find(self.client_id).client_name
    end
  end
end
