class Client < ApplicationRecord
   validates :client_name, :street_address, :street_city, :street_state, :street_postcode, :contact_name, :contact_email, :contact_phone, :client_abn, presence: true

   has_many :invoice_payments
   has_many :invoices
   has_many :bookings

   def display_address
     if self.street_address != nil then
       return self.street_address + ", " + self.street_city + ", " + self.street_state + ", " + self.street_postcode
     end
   end
   def display_post_address
     if self.postal_address != nil then
       return self.postal_address + ", " + self.postal_city + ", " + self.postal_state + ", " + self.postal_postcode
     end
   end

end
