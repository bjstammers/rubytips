class Employee < ApplicationRecord
    validates :first_name, :surname, :full_name, :address, :city, :postcode, :state, :home_phone, :mobile_phone, presence: true

    has_many :payments
    has_many :invoice_details
    has_many :b_details

    def display_name
        if self.known_as == ""
            return self.first_name + " " + self.surname
        else
            return self.known_as + " " + self.surname
        end
    end

    def display_address
        return self.address + ", " + self.city + ", " + self.state + " " + self.postcode
    end

    def current_yearly_earnings

    end

    def current_yearly_payments

    end
end
