class Invoice < ApplicationRecord
  validates :invoice_ref, :invoice_date, :client_id, :booking_id, :super_total, :gst_included, :invoice_total, presence: :true

  has_many :invoice_details, dependent: :destroy
  belongs_to :client
  belongs_to :booking

  accepts_nested_attributes_for :invoice_details
  
  def client_name
    if self.client_id != "" then
      return Client.find(self.client_id).client_name
    end
  end
  def booking_ref
    if self.booking_id != "" then
      return Booking.find(self.booking_id).booking_ref
    end
  end
  def booking_date
    if self.booking_id != "" then
      return Booking.find(self.booking_id).start_date
    end
  end
  def booking_desc
    if self.booking_id != "" then
      return Booking.find(self.booking_id).description
    end
  end

end
