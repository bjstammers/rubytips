json.extract! booking, :id, :booking_ref, :created_at, :updated_at
json.url booking_url(booking, format: :json)
