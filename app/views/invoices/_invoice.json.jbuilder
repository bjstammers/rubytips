json.extract! invoice, :id, :invoice_ref, :invoice_date, :client_id, :booking_id, :super_total, :service_fee, :gst_included, :gst_service_fee, :gst_invoice, :invoice_currency, :invoice_total, :print_flag, :paid_flag, :current_gross, :current_gst, :created_at, :updated_at
json.url invoice_url(invoice, format: :json)
