json.extract! admin_keyword, :id, :type, :subtype, :value, :created_at, :updated_at
json.url admin_keyword_url(admin_keyword, format: :json)
