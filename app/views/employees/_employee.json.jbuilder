json.extract! employee, :id, :first_name, :middle_name, :surname, :full_name, :known_as, :address, :city, :state, :postcode, :home_phone, :work_phone, :mobile_phone, :email, :birth_date, :dob_display, :superannuation_fund, :superannuation_no, :tax_file_number, :abn, :gst_registered, :created_at, :updated_at
json.url employee_url(employee, format: :json)
