class AdminKeywordsController < ApplicationController
  before_action :set_admin_keyword, only: [:show, :edit, :update, :destroy]

  # GET /admin_keywords
  # GET /admin_keywords.json
  def index
    @keywords = AdminKeyword.all
  end

  # GET /admin_keywords/1
  # GET /admin_keywords/1.json
  def show
  end

  # GET /admin_keywords/new
  def new
    @keyword = AdminKeyword.new
  end

  # GET /admin_keywords/1/edit
  def edit
  end

  # POST /admin_keywords
  # POST /admin_keywords.json
  def create
    @keyword = AdminKeyword.new(admin_keyword_params)

    respond_to do |format|
      if @keyword.save
        format.html { redirect_to admin_keywords_url, notice: 'Keyword was successfully created.' }
        format.json { render :show, status: :created, location: @keyword }
      else
        format.html { render :new }
        format.json { render json: @keyword.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin_keywords/1
  # PATCH/PUT /admin_keywords/1.json
  def update
    respond_to do |format|
      if @keyword.update(admin_keyword_params)
        format.html { redirect_to @keyword, notice: 'Keyword was successfully updated.' }
        format.json { render :show, status: :ok, location: @keyword }
      else
        format.html { render :edit }
        format.json { render json: @keyword.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin_keywords/1
  # DELETE /admin_keywords/1.json
  def destroy
    @keyword.destroy
    respond_to do |format|
      format.html { redirect_to admin_keywords_url, notice: 'Keyword was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_keyword
      @keyword = AdminKeyword.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_keyword_params
      params.require(:admin_keyword).permit(:key_type, :key_subtype, :key_value)
    end
end
