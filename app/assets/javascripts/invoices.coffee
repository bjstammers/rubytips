# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
    $('#invoice_booking_id').on 'change', ->
        alert 'booking selected'
        alert 'Booking ID = ' + $(this).val()
        path = '/bookings/' + $(this).val() + '/search'
        $.get path, (data) ->
            rtn = JSON.stringify(data)
            tmp = rtn.slice(rtn.indexOf("<div>client_name:") + 17)
            client = tmp.substr(0, tmp.indexOf("</div>")) 
            tmp = rtn.slice(rtn.indexOf("<div>start_date:") + 16)
            date = tmp.substr(0, tmp.indexOf("</div>")) 
            tmp = rtn.slice(rtn.indexOf("<div>description:") + 17)
            desc = tmp.substr(0, tmp.indexOf("</div>")) 
            $('#dspClientName').html(client)
            $('#dspBookingDate').html(date)
            $('#dspDescription').html(desc)
            return
    return