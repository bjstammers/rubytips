# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  date_input = $('input[id="employee_birth_date"]')
  container = if $('.tab-content form-group col-sm-2').length > 0 then $('.tab-content form-group col-sm-2').parent() else 'body'
  options =
    format: 'dd/mm/yyyy'
    container: container
    todayHighlight: true
    autoclose: true
  date_input.datepicker options
  return
